Laundry4::Application.routes.draw do
  get 'users/create'
  get 'users' => "users#index"
  get 'orders' => "users#index"
  get 'users/today' => "users#today"
  get 'users/month' => "users#month"
  get 'users/week' => "users#week"
  get 'orders/:id' => "users#show"
  get 'orders/:id/tab' => 'users#tab', as: :tab
  get 'orders/:id/sendmes' => "users#sendmes", as: :sendmes
  get 'orders/:id/sendmes_fin' => "users#sendmes_fin", as: :sendmes_fin
  match 'user_update_status/:id' => "users#update_status", as: :update_user, via: :put
  match 'orders/:id' => 'users#destroy', via: :delete
  match "user_new" => "users#new", via: :get
  match 'users/create' => "users#create", as: :create_user, via: :post
  resources :users
  resources :orders
  resources :item_temps
  resources :items

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'users#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
