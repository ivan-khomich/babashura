class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.string :keyword
      t.integer :cost_student
      t.integer :cost

      t.timestamps
    end
  end
end
