class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :user, index: true
      t.boolean :status
      t.text :other
      t.integer :sum
      t.boolean :nal
      t.integer :zagr_stirka
      t.integer :zagr_sushka

      t.timestamps
    end
  end
end
