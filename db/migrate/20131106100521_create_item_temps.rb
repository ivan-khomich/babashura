class CreateItemTemps < ActiveRecord::Migration
  def change
    create_table :item_temps do |t|
      t.references :item, index: true
      t.references :order, index: true

      t.timestamps
    end
  end
end
