class Item < ActiveRecord::Base
  has_many :item_temps
  has_many :orders, through: :item_temps
end
