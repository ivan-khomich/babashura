class User < ActiveRecord::Base
  has_many :orders
  validates :name, :surname, presence: true
  validates :phone, length: { is: 10 }

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |product|
        csv << product.attributes.values_at(*column_names)
      end
    end
  end
end

