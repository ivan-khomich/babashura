class Order < ActiveRecord::Base
  belongs_to :user
  has_many :item_temps
  has_many :items, through: :item_temps



  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |product|
        csv << product.attributes.values_at(*column_names)
      end
    end
  end
end
