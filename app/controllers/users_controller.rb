class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :update_status, :sendmes, :tab, :sendmes_fin]
  before_action :set_hash, only: [:edit, :new, :index, :create, :update, :show, :today, :week, :month]
  before_action :hash_numbers, only: [:edit, :new, :index, :create, :update, :show, :today, :week, :month]
  
  def index
    @orders = Order.paginate(:page => params[:page], :per_page => 15)
    @orders_all = Order.all
    respond_to do |format|
      format.html
      format.xls
      format.xml  { render :xml => @orders_all }
    end
  end

  def show
  end
  
  def today
    @orders = Order.where(created_at: (Time.now.midnight)..Time.now.midnight + 1.day).paginate(:page => params[:page], :per_page => 15)
    respond_to do |format|
      format.html
      format.xls
      format.xml  { render :xml => @orders_all }
    end
  end

  def week
    @orders = Order.where(created_at: (Time.now.midnight)-7.days..Time.now.midnight + 1.day).paginate(:page => params[:page], :per_page => 15)

    respond_to do |format|
      format.html
      format.xls
      format.xml  { render :xml => @orders_all }
    end
  end

  def month
    @orders = Order.where(created_at: (Time.now.midnight)-1.month ..Time.now.midnight + 1.day).paginate(:page => params[:page], :per_page => 15)
    respond_to do |format|
      format.html
      format.xls
      format.xml  { render :xml => @orders_all }
    end
  end
  
  def tab
  end
  
  def new
    @user = User.new
    @order = Order.new
    @item_temp = ItemTemp.new
  end
  # POST /users
  # POST /users.json
  
  def create
    if User.find_by(phone: user_params["phone"]).nil?
      @user = User.new(user_params)
    else
      @user = User.find_by(phone: user_params["phone"])
    end
    @order = Order.new(order_params)
    @order.sum = 0
    params[:items].each do |key, value|
      @item_temp = ItemTemp.new(:item_id => value)
      @order.item_temps.push @item_temp
      if (@user.status) then
        @order.sum += Item.find(value).cost_student
      else
        @order.sum += Item.find(value).cost
      end
    end
    if (@user.status) then
      @order.sum += Item.find(1).cost_student * @order.zagr_stirka
      @order.sum += Item.find(2).cost_student * @order.zagr_sushka
    else
      @order.sum +=Item.find(1).cost * @order.zagr_stirka
      @order.sum += Item.find(2).cost * @order.zagr_sushka
    end

    @user.orders.push @order

    @user.save
    respond_to do |format|
      if @user.save
        format.html { redirect_to @order }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @order.destroy
    @order.item_temps.each do |f|
      f.destroy
    end
    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end
  
  def edit
  end

  def update
    respond_to do |format|
      if @user.update(user_params) && @order.update(order_params) do
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      end
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update_status
    @order.update(status: true)
    @order.save

    redirect_to(:action => :sendmes_fin)
  end

  def sendmes_fin
    message = MainsmsApi::Message.new(sender: 'sendertest', message: "Vash zakaz gotov. Mozhete zabirat`. Prachechnaya.", recipients: ['8'+@order.user.phone.to_s])
    response = message.deliver

    redirect_to(:action => :index)
  end

  def sendmes
    message = MainsmsApi::Message.new(sender: 'sendertest', :message => "TEXT", recipients: ['8'+@order.user.phone.to_s])
    response = message.deliver

    redirect_to(:action => :show)
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @order = Order.find(params[:id])
    end
    def hash_numbers
      @users = User.all
      @phones = Array.new
      @users.each do |user|
        @phones.push user.phone
      end
      gon.phones = @phones
    end

    def set_hash
      @Item_hash = Hash.new()
      @Item_hash[:stirka] = Item.where(keyword: "stirka").to_a
      @Item_hash[:glazhka] = Item.where(keyword: "glazhka").to_a
      @Item_hash[:sushka] = Item.where(keyword: "sushka").to_a
      @Item_hash[:poroshok] = Item.where(keyword: "poroshok").to_a
      gon.sdf = @Item_hash.to_json
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require("user").permit("name", "surname", "phone", "status")
    end

    def order_params
      params.require("order").permit("user_id", "status", "other", "nal", "zagr_stirka", "zagr_sushka")
    end
end