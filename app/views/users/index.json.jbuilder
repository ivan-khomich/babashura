json.array!(@users) do |user|
  json.extract! user, :name, :surname, :phone
  json.url user_url(user, format: :json)
end
