function status_hide() {
	var arr = document.getElementsByClassName('status_finish');
	var i=0;
	while(arr[i]!=undefined) {
		arr[i].parentNode.parentNode.parentNode.style.display = "none";
		i++;
	}
}
function status_show() {
	var arr = document.getElementsByClassName('status_finish');
	var i=0;
	while(arr[i]!=undefined) {
		arr[i].parentNode.parentNode.parentNode.removeAttribute('style');
		i++;
	}
}