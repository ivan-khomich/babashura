function buttons_first() {
	var butt_plus = document.getElementsByClassName('butt_plus');
	var butt_minus = document.getElementsByClassName('butt_minus');
	var k = 0;
	while(butt_plus[k]!=undefined) {
		butt_plus[k].onclick = function() {
			var val = this.parentNode.parentNode.getElementsByClassName('form-control')[0];
			val.value = parseInt(val.value, 10) + 1;
			val.setAttribute('value', parseInt(val.value, 10));
		}
		butt_minus[k].onclick = function() {
			var val = this.parentNode.parentNode.getElementsByClassName('form-control')[0];
			if(parseInt(val.value)!=0) {
				val.value = parseInt(val.value, 10) - 1;
				val.setAttribute('value', parseInt(val.value, 10));
			}
		}
		k++;
	}
}
function button_second () {
	var butt_plus = document.getElementsByClassName('butt_sec_plus');
	var butt_minus = document.getElementsByClassName('butt_sec_minus');
	var k = 0;
	while(butt_plus[k]!=undefined) {
		butt_plus[k].onclick = function() {
			var val = this.parentNode.parentNode.getElementsByClassName('form-control')[0];
			val.value = parseInt(val.value, 10) + 1;
			val.setAttribute('value', parseInt(val.value, 10));
			var elt = document.createElement('input');
			var length = document.getElementsByClassName('sender').length;

			elt.setAttribute('name', 'items['+length+']');
			elt.setAttribute('class','sender');
			elt.setAttribute('style', 'display:none');
			var temp_name = this.parentNode.parentNode.parentNode.getElementsByClassName('item_id')[0].getAttribute('data-text');
			for (var p in temp_array) {
				var i =0;
				while(temp_array[p][i]!=undefined) {
					console.log(temp_array[p][i]['name']);
					if(temp_array[p][i]['name']==temp_name) {
						var lova = temp_array[p][i]['id'];
						console.log(lova);
					}
					i++;
				}
			}
			elt.setAttribute('value', lova);
			this.parentNode.parentNode.appendChild(elt);
		}
		butt_minus[k].onclick = function() {
			var val = this.parentNode.parentNode.getElementsByClassName('form-control')[0];
			if(parseInt(val.value)!=0) {
				val.value = parseInt(val.value, 10) - 1;
				val.setAttribute('value', parseInt(val.value, 10));
				length = document.getElementsByClassName('sender').length;
				this.parentNode.parentNode.removeChild(this.parentNode.parentNode.getElementsByClassName('sender')[length-1]);
			}
		}
		k++;
	}
}

function accordion () {
	$("#accordion").accordion({ heightStyle: "content", active: true,collapsible: true });
};
function phones() {
	availableTags = gon.phones
	$( "#autocomplete" ).autocomplete({
			source: availableTags
		});
}
function union() {
	temp_array = JSON.parse(gon.sdf);
	phones();
	accordion();
	buttons_first();
	button_second();
}

$(document).ready(union);
$(document).on('page:load', union);