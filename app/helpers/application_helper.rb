module ApplicationHelper
   def items_count(id, order)
    i = 0
    order.items.each do |item|
      if item.id == id
        i += 1
      end
    end
    return i
  end
end
